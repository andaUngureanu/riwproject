package riw.indexer;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import riw.database.DBManager;

public class InvertedIndexer {
	private String interestWord = null;

	public InvertedIndexer(String interestWord) {
		this.interestWord = interestWord;
	}

	public void invert() {
		MongoCollection<Document> directindex = DBManager.getDirectindex();
		MongoCollection<Document> invertedindex = DBManager.getInvertedindex();

		Document docToInsert = new Document("term", interestWord);
		List<Document> docsInfo = new ArrayList<Document>();
		BasicDBObject searchQuery = new BasicDBObject();
		BasicDBObject projection = new BasicDBObject(
				new BasicDBObject("terms", new BasicDBObject("$elemMatch", new BasicDBObject("t", interestWord))));
		searchQuery.put("terms.t", interestWord);
		projection.put("_id", 0);
		projection.put("doc", 1);
		MongoCursor<Document> docs = directindex.find(searchQuery).projection(projection).iterator();
		while (docs.hasNext()) {
			Document doc = docs.next();
			int docKey = doc.getInteger("doc");
			List<Document> listDocs = (List<Document>) doc.get("terms");

			double count = listDocs.get(0).getDouble("c");
			Document docInfo = new Document("d", docKey).append("c", count);
			docsInfo.add(docInfo);

		}
		docToInsert.append("docs", docsInfo);
		invertedindex.insertOne(docToInsert);
	}
}
