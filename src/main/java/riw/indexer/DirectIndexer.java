package riw.indexer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.mongodb.client.MongoCollection;

import riw.database.DBManager;
import riw.parser.WordCounter;

public class DirectIndexer {
	List<File> workFiles;
	List<Integer> nrOfWords;
	int startIdx;

	public DirectIndexer(List<File> files, int startIdx) {
		this.workFiles = files;
		nrOfWords = new ArrayList<Integer>();
		this.startIdx = startIdx;
	}

	public void writeFilesMapToDB() {
		MongoCollection<Document> filesmap = DBManager.getFilesMap();
		List<Document> fileKeys = new ArrayList<Document>();
		for (int i = 0; i < workFiles.size(); ++i) {
			Document fileKey = new Document("key", i + startIdx);
			fileKey.append("path", workFiles.get(i).getAbsolutePath());
			fileKey.append("count", nrOfWords.get(i));
			fileKeys.add(fileKey);
		}
		if (!fileKeys.isEmpty()) {
			filesmap.insertMany(fileKeys);
		}
	}

	public void writeDIdxToDB() {
		Map<String, Integer> wordsMap = null;
		MongoCollection<Document> collection = DBManager.getDirectindex();
		List<Document> documentsToInsert = new ArrayList<Document>();
		for (int i = 0; i < workFiles.size(); ++i) {
			int nrOfWordsInFile = 0;
			WordCounter wordCounter = new WordCounter();
			nrOfWordsInFile = wordCounter.ComputeTextFile(workFiles.get(i).getAbsolutePath());
			wordsMap = wordCounter.getCountedWords();
			// wordsMap =
			// DocumentReader.computeTextFile(workFiles.get(i).getAbsolutePath(),
			// nrOfWordsInFile);
			// System.out.println(workFiles.get(i).getAbsolutePath() + " - " +
			// nrOfWordsInFile);
			nrOfWords.add(nrOfWordsInFile);
			Document rootDoc = new Document("doc", i + startIdx);
			List<Document> wordsInfo = new ArrayList();
			for (Map.Entry<String, Integer> wordEntry : wordsMap.entrySet()) {
				wordsInfo.add(new Document("t", wordEntry.getKey()).append("c",
						wordEntry.getValue() * 1.0 / nrOfWordsInFile));
			}
			rootDoc.append("terms", wordsInfo);
			documentsToInsert.add(rootDoc);
			// collection.insertOne(rootDoc);

		}
		collection.insertMany(documentsToInsert);
	}

}
