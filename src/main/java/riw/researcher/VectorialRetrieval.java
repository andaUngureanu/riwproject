package riw.researcher;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import riw.utile.ValueComparator;
import riw.utile.WordWithCoef;

public class VectorialRetrieval {
	BooleanRetrieval booleanRetrieval = null;
	Map<String, Double> queryCoef = null;
	Map<Integer, Set<WordWithCoef>> result = null;

	Map<Integer, Double> resultsInOrder = null;

	public VectorialRetrieval(String query, int nrOfFiles) {
		booleanRetrieval = new BooleanRetrieval(query, nrOfFiles);
		Set<Integer> fileKeySet = booleanRetrieval.executeQuery();
		result = booleanRetrieval.getResult(fileKeySet);
		queryCoef = new TreeMap();
		resultsInOrder = new TreeMap<Integer, Double>();
		computeQuery();
	}

	private void computeQuery() {
		List<String> wordsInQuery = booleanRetrieval.getWordsInQuery();
		int nrOfWordsInQuery = wordsInQuery.size();
		Map<String, Double> queryWordsCount = new TreeMap();
		for (String w : wordsInQuery) {
			if (!queryWordsCount.containsKey(w)) {
				queryWordsCount.put(w, 1.0);
			} else {
				double coef = queryWordsCount.get(w);
				queryWordsCount.replace(w, coef + 1);
			}
		}
		for (Map.Entry<String, Double> wordInQuery : queryWordsCount.entrySet()) {
			double idf = booleanRetrieval.getIdfQueryWords(wordInQuery.getKey());
			double coef = idf * wordInQuery.getValue() / nrOfWordsInQuery;
			queryCoef.put(wordInQuery.getKey(), coef);
		}

		for (Map.Entry<Integer, Set<WordWithCoef>> entry : result.entrySet()) {
			double value = computeCosSimilarity(queryCoef, entry.getValue());
			resultsInOrder.put(entry.getKey(), value);
		}
		ValueComparator valC = new ValueComparator(resultsInOrder);
		Map<Integer, Double> res = new TreeMap<Integer, Double>(valC);
		res.putAll(resultsInOrder);
		System.out.println("vr-> " + res + " <-vr");
	}

	public double normOfFile(Set<WordWithCoef> d) {
		double norm = 0;
		for (WordWithCoef w : d) {
			norm += Math.pow(w.getCount(), 2);
		}
		norm = Math.sqrt(norm);
		return norm;
	}

	public double normOfFile(Map<String, Double> q) {
		double norm = 0;
		for (Map.Entry<String, Double> w : q.entrySet()) {
			norm += Math.pow(w.getValue(), 2);
		}
		norm = Math.sqrt(norm);
		return norm;
	}

	public double computeCosSimilarity(Map<String, Double> q, Set<WordWithCoef> d) {
		double result = 0;
		double normQ = normOfFile(q);
		double normD = normOfFile(d);

		for (WordWithCoef w : d) {
			result += w.getCount() * q.get(w.getWord());
		}
		return result / (normQ * normD);
	}

}
