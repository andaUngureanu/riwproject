package riw.researcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;

import riw.database.DBManager;

public class FileKeySolver {
	public FileKeySolver() {

	}

	public static List<String> extractFilesWithKeysFromDB(Set<Integer> i) {
		MongoCollection<Document> filesmap = DBManager.getFilesMap();
		List<String> paths = new ArrayList<String>();
		if (i == null)
			return null;
		for (Integer key : i) {
			BasicDBObject searchQueryi = new BasicDBObject();
			searchQueryi.put("key", key);
			Document doc = filesmap.find(searchQueryi).first();
			String path = doc.getString("path");
			paths.add(path);
		}
		return paths;
	}

}
