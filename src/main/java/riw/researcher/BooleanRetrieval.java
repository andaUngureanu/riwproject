package riw.researcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.bson.Document;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;

import riw.analist.WordAnalist;
import riw.database.DBManager;
import riw.parser.WordCounter;
import riw.utile.WordWithCoef;

public class BooleanRetrieval {
	int nrOfFiles;
	MongoCollection<Document> invertedindex;
	Map<Integer, Set<WordWithCoef>> fileInfosMap;
	Map<String, Set<Integer>> wordFilesMap;
	String input;
	List<String> wordsInQuery;
	List<String> operations;

	public double getIdfQueryWords(String word) {

		double idf = wordFilesMap.get(word) != null ? wordFilesMap.get(word).size() : 0;
		if (idf != 0)
			idf = Math.log(nrOfFiles / idf);
		return idf;
	}

	public BooleanRetrieval(String query, int nrOfFiles) {
		this.nrOfFiles = nrOfFiles;
		invertedindex = DBManager.getInvertedindex();
		input = query;
		wordsInQuery = new ArrayList();
		operations = new ArrayList();
		wordFilesMap = new TreeMap();
		fileInfosMap = new HashMap();
		WordCounter.ComputeTextInput(input, wordsInQuery, operations);
		populateWordFilesMap();

	}

	public void getQuery(String input) {

		String text = input.toLowerCase();
		String word = "";
		int startIdx = 0, endIdx = 0;
		for (int i = 0; i < text.length(); ++i) {
			if (!((text.charAt(i) >= 'a' && text.charAt(i) <= 'z')
					|| (text.charAt(i) >= '0' && text.charAt(i) <= '9'))) {
				endIdx = i;
				word = text.substring(startIdx, endIdx);
				if (WordAnalist.isOperator(word)) {
					operations.add(word);
				} else if (WordAnalist.isExceptionWord(word)) {
					wordsInQuery.add(word);
				} else if (endIdx - startIdx > 2 && (!WordAnalist.isStopWord(word))) {
					wordsInQuery.add(word);
				}
				startIdx = i + 1;
			}
		}
		word = text.substring(startIdx, text.length());
		if (WordAnalist.isExceptionWord(word)) {
			wordsInQuery.add(word);
		} else if (text.length() - startIdx > 2 && (!WordAnalist.isStopWord(word))) {
			wordsInQuery.add(word);
		}

	}

	/*
	 * public Set<FileKeyAndCount> extractFileInfosFromDB(String word) {
	 * Set<FileKeyAndCount> fileKeys = new TreeSet(); MongoCollection<Document>
	 * invertedindex = DBManager.getInvertedindex(); BasicDBObject query = new
	 * BasicDBObject(); query.put("term", word); BasicDBObject projection = new
	 * BasicDBObject(); projection.put("_id", 0); projection.put("docs", 1);
	 * Document documents =
	 * invertedindex.find(query).projection(projection).first(); if (documents
	 * == null) return null; List<Document> docs = (List<Document>)
	 * documents.get("docs");
	 * 
	 * for (Document d : docs) {
	 * 
	 * fileKeys.add(new FileKeyAndCount(d.getInteger("d")); } return fileKeys; }
	 */

	public Set<Integer> extractFilesFromDB(String word) {
		Set<Integer> fileKeys = new TreeSet();
		MongoCollection<Document> invertedindex = DBManager.getInvertedindex();
		BasicDBObject query = new BasicDBObject();
		query.put("term", word);
		BasicDBObject projection = new BasicDBObject();
		projection.put("_id", 0);
		projection.put("docs", 1);
		Document documents = invertedindex.find(query).projection(projection).first();
		if (documents == null)
			return null;
		List<Document> docs = (List<Document>) documents.get("docs");
		double idf = Math.log(nrOfFiles * 1.0 / docs.size());
		for (Document d : docs) {
			int fileKey = d.getInteger("d");
			// System.out.println(d.toJson());
			double coef = d.getDouble("c") * idf;
			if (fileInfosMap.containsKey(fileKey)) {
				Set<WordWithCoef> wordsWithCoef = fileInfosMap.get(fileKey);
				wordsWithCoef.add(new WordWithCoef(word, coef));
				fileInfosMap.replace(fileKey, wordsWithCoef);
			} else {
				Set<WordWithCoef> wordsWithCoef = new TreeSet<WordWithCoef>();
				wordsWithCoef.add(new WordWithCoef(word, coef));
				fileInfosMap.put(fileKey, wordsWithCoef);
			}
			fileKeys.add(fileKey);
		}
		return fileKeys;
	}

	public void populateWordFilesMap() {
		for (String word : wordsInQuery) {
			Set<Integer> value = extractFilesFromDB(word);
			wordFilesMap.put(word, value);
		}
		// System.out.println("**" + wordFilesMap + "**");
		// System.out.println(fileInfosMap);
	}

	public Set<Integer> and(String w1, String w2) {
		Set<Integer> v1 = wordFilesMap.get(w1);
		Set<Integer> v2 = wordFilesMap.get(w2);
		Set<Integer> result = new TreeSet();
		if (v1 == null || v2 == null)
			return null;

		if (v1.size() < v2.size()) {
			result.addAll(v1);
			result.retainAll(v2);
		} else {
			result.addAll(v2);
			result.retainAll(v1);
		}
		return result;

	}

	public Set<Integer> or(String w1, String w2) {
		Set<Integer> v1 = wordFilesMap.get(w1);
		Set<Integer> v2 = wordFilesMap.get(w2);
		Set<Integer> result = new TreeSet();

		if (v1 == null) {
			return v2;
		}
		if (v2 == null) {
			return v1;
		}
		if (v1.size() > v2.size()) {
			result.addAll(v1);
			result.addAll(v2);
		} else {
			result.addAll(v2);
			result.addAll(v1);
		}
		return result;
	}

	public Set<Integer> not(String w1, String w2) {
		Set<Integer> v1 = wordFilesMap.get(w1);
		Set<Integer> v2 = wordFilesMap.get(w2);
		Set<Integer> result = new TreeSet();
		if (v1 == null) {
			return null;
		}
		if (v2 == null) {
			return v1;
		}
		for (Integer val : v1) {
			if (!v2.contains(val))
				result.add(val);
		}

		return result;
	}

	public Set<Integer> and(Set<Integer> v1, String w2) {
		Set<Integer> v2 = wordFilesMap.get(w2);
		if (v1 == null || v2 == null)
			return null;
		Set<Integer> result = new TreeSet();
		if (v1.size() < v2.size()) {
			result.addAll(v1);
			result.retainAll(v2);
		} else {
			result.addAll(v2);
			result.retainAll(v1);
		}
		return result;
	}

	public Set<Integer> or(Set<Integer> v1, String w2) {
		Set<Integer> v2 = wordFilesMap.get(w2);
		Set<Integer> result = new TreeSet();
		if (v1 == null) {
			return v2;
		}
		if (v2 == null) {
			return v1;
		}
		if (v1.size() > v2.size()) {
			result.addAll(v1);
			result.addAll(v2);
		} else {
			result.addAll(v2);
			result.addAll(v1);
		}
		return result;
	}

	public Set<Integer> not(Set<Integer> v1, String w2) {
		Set<Integer> v2 = wordFilesMap.get(w2);
		Set<Integer> result = new TreeSet();
		if (v1 == null) {
			return null;
		}
		if (v2 == null) {
			return v1;
		}
		for (Integer val : v1) {
			if (!v2.contains(val))
				result.add(val);
		}

		return result;
	}

	public Set<Integer> executeQuery() {
		Set<Integer> result = null;
		int idx = 1;
		boolean areWords = true;
		if (wordsInQuery.size() == 0)
			return null;
		String w1 = wordsInQuery.get(idx - 1);
		String w2;

		for (String operation : operations) {
			w2 = wordsInQuery.get(idx);
			switch (operation) {
			case "and":
				if (!areWords) {
					result = and(result, w2);
				} else {
					result = and(w1, w2);
					areWords = false;
				}
				break;
			case "or":
				if (!areWords) {
					result = or(result, w2);
				} else {
					result = or(w1, w2);
					areWords = false;
				}
				break;
			case "not":
				if (!areWords) {
					result = not(result, w2);
				} else {
					result = not(w1, w2);
					areWords = false;
				}
				break;
			}
			idx++;

		}
		return result;
	}

	public Map<Integer, Set<WordWithCoef>> getResult(Set<Integer> fileKeys) {
		Map<Integer, Set<WordWithCoef>> result = new TreeMap();
		for (Integer i : fileKeys) {
			result.put(i, new TreeSet(fileInfosMap.get(i)));

		}

		return result;
	}

	public List<String> getWordsInQuery() {
		return wordsInQuery;
	}

}
