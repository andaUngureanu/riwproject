package riw.utile;

public class WordWithCoef implements Comparable<WordWithCoef> {
	private String word;
	private double count;

	public WordWithCoef(String word, double count) {
		super();
		this.word = word;
		this.count = count;
	}

	/**
	 * @return the word
	 */
	public String getWord() {
		return word;
	}

	/**
	 * @param word
	 *            the word to set
	 */
	public void setWord(String word) {
		this.word = word;
	}

	/**
	 * @return the count
	 */
	public double getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public int compareTo(WordWithCoef o) {

		return word.compareTo(o.word);
	}

	@Override
	public String toString() {
		return word + "=" + count + " ";
	}

}
