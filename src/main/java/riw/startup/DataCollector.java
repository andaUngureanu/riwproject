package riw.startup;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import riw.database.DBManager;
import riw.indexer.InvertedIndexer;
import riw.indexer.DirectIndexer;
import riw.parser.DocumentReader;

public class DataCollector {
	String path = "";
	List<String> suffixes = null;
	List<File> workingFiles = null;
	List<File> parsedFiles = null;
	int diNrOfFiles = 0;
	List<File> mapFiles = new ArrayList<File>();
	List<File> diFiles = new ArrayList<File>();
	List<File> invFiles = new ArrayList<File>();
	File dir = new File("D:/JavaProjectsIII/RIW/output/di/");

	int idxsCount = 0;

	// deprecated -used in initial implementation
	public void processingFile(File file) {
		// thread pool thread pool executer
		// TODO: each processing file should be on a different thread

		// create the .reader and .counter file
		DocumentReader docReader = new DocumentReader(file.getAbsolutePath(), null, "");// start
																						// neblocant
		String suffixFit = "";
		for (String suffix : suffixes) {
			if (file.getAbsolutePath().endsWith(suffix)) {
				suffixFit = suffix;
			}
		}
		String readerOutput = file.getAbsolutePath().replace(suffixFit, ".reader");
		// docReader.WriteData(readerOutput);
		parsedFiles.add(file);

	}

	public DataCollector(String path, int diNrOfFiles) {
		this.diNrOfFiles = diNrOfFiles;
		this.path = path;
		File inputFile = new File(path);
		parsedFiles = new ArrayList<File>();

		workingFiles = getFilesFromPath(inputFile);
		// create direct indexers with map for groups of <diNrOfFiles> files
		for (File file : workingFiles) {
			parsedFiles.add(file);
			if (parsedFiles.size() >= diNrOfFiles * (idxsCount + 1)) {
				idxsCount++;
				DirectIndexer directIdx = new DirectIndexer(
						parsedFiles.subList(diNrOfFiles * (idxsCount - 1), diNrOfFiles * idxsCount),
						(idxsCount - 1) * diNrOfFiles);

				directIdx.writeDIdxToDB();
				directIdx.writeFilesMapToDB();
			}
		}

		if (parsedFiles.size() > idxsCount * diNrOfFiles) {
			idxsCount++;

			DirectIndexer directIdx = new DirectIndexer(
					parsedFiles.subList((idxsCount - 1) * diNrOfFiles, parsedFiles.size()),
					(idxsCount - 1) * diNrOfFiles);

			directIdx.writeDIdxToDB();
			directIdx.writeFilesMapToDB();
		}

		System.out.println("Created direct index and files map> " + new Date());
		// invertedIndexer
		MongoCollection<Document> directindex = DBManager.getDirectindex();
		MongoCursor<String> mc = directindex.distinct("terms.t", String.class).iterator();
		while (mc.hasNext()) {
			String word = mc.next();
			InvertedIndexer invertedIndexer = new InvertedIndexer(word);
			invertedIndexer.invert();
		}

		System.out.println("Created inverted index> " + new Date());
	}

	// deprecated - used in the first implementation
	public DataCollector(String path, List<String> suffixes, int diNrOfFiles) {
		this.diNrOfFiles = diNrOfFiles;
		this.path = path;
		File inputFile = new File(path);
		this.suffixes = new ArrayList<String>(suffixes);
		parsedFiles = new ArrayList<File>();
		// iiData = new TreeMap();
		workingFiles = getFilesWithEndings(inputFile, suffixes);
		// create folder for directIndexer output
		if (!dir.exists()) {
			dir.mkdir();
		}

		// create direct index with map for each 5 files
		for (File file : workingFiles) {
			processingFile(file);
			if (parsedFiles.size() >= diNrOfFiles * (idxsCount + 1)) {
				idxsCount++;
				File mapFile = new File(dir.getAbsolutePath() + "/" + idxsCount + ".dim");
				File diFile = new File(dir.getAbsolutePath() + "/" + idxsCount + ".di");
				// DirectIndexer directIdx = new DirectIndexer(
				// parsedFiles.subList(diNrOfFiles * (idxsCount - 1),
				// diNrOfFiles * idxsCount), mapFile, diFile,
				// (idxsCount - 1) * diNrOfFiles);

				// directIdx.writeFilesMapToDB();
				// directIdx.writeDIdxToDB();
			}
		}
		if (parsedFiles.size() > idxsCount * diNrOfFiles) {
			idxsCount++;
			File mapFile = new File(dir.getAbsolutePath() + "/" + idxsCount + ".dim");
			File diFile = new File(dir.getAbsolutePath() + "/" + idxsCount + ".di");
			// DirectIndexer directIdx = new DirectIndexer(
			// parsedFiles.subList((idxsCount - 1) * diNrOfFiles,
			// parsedFiles.size()), mapFile, diFile,
			// (idxsCount - 1) * diNrOfFiles);

			// directIdx.writeFilesMapToDB();
			// directIdx.writeDIdxToDB();
		}

		// invertedIndexer
		File[] DIFiles = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".di");
			}
		});
		int n = 0;
		List<File> IIFiles = new ArrayList();
		for (File file : DIFiles) {
			// invertedIndexer file
			n++;
			File iiFile = new File(dir.getAbsolutePath() + "/" + n + ".ii");
			// InvertedIndexer ii = new InvertedIndexer(file, iiFile);
			IIFiles.add(iiFile);
		}
		for (File iiFile : IIFiles) {
			// readIIFile(iiFile);
		}

		// InvertedIndexer.writeDataToFile(iiData, new
		// File(dir.getAbsoluteFile() + "/" + "IndexInvers"));
		// InvertedIndexer.insertDataToDB(iiData);
	}

	public List<File> getParsedFiles() {
		return parsedFiles;
	}

	public int getTotalFilesNumber() {
		return parsedFiles.size();
	}

	private List<File> getFilesFromPath(File inputPath) {
		List<File> files = new ArrayList<File>();
		List<File> directories = new ArrayList<File>();
		if (inputPath.isDirectory())
			directories.add(inputPath);
		for (int i = 0; i < directories.size(); ++i) {
			File dir = directories.get(i);
			String[] dirContent = dir.list();

			for (int j = 0; j < dirContent.length; ++j) {
				File file = new File(directories.get(i).getAbsolutePath(), dirContent[j]);
				if (file.isDirectory()) {
					directories.add(file);
				} else {
					files.add(file);
				}
			}
		}
		return files;
	}

	// deprecated - used in initial implementation
	private List<File> getFilesWithEndings(File inputFile, List<String> suffixes) {
		List<File> files = new ArrayList<File>();
		List<File> directories = new ArrayList<File>();
		// check if input is directory or file
		if (inputFile.isDirectory())
			directories.add(inputFile);

		// iterate through directories list
		for (int i = 0; i < directories.size(); i++) {
			File dir = directories.get(i);
			String[] dirContent = dir.list();
			// iterate through directory's content
			for (int j = 0; j < dirContent.length; j++) {
				File file = new File(directories.get(i).getAbsolutePath(), dirContent[j]);
				// check if content is directory
				if (file.isDirectory()) {
					directories.add(file);
				} else {
					// if content is file and it should be computed add to files
					for (String suffix : suffixes) {
						if (file.getAbsolutePath().endsWith(suffix)) {
							files.add(file);
							System.out.println(file.getPath());
							// processingFile(file);
						}
					}
				}
			}
		}
		return files;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path
	 *            the path to set
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * @return the htmFiles
	 */
	public List<File> getHtmFiles() {
		return workingFiles;
	}

	/**
	 * @param htmFiles
	 *            the htmFiles to set
	 */
	public void setHtmFiles(List<File> htmFiles) {
		this.workingFiles = htmFiles;
	}

}
