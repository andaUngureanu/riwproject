package riw.startup;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import riw.analist.WordAnalist;
import riw.researcher.BooleanRetrieval;
import riw.researcher.FileKeySolver;
import riw.researcher.VectorialRetrieval;
import riw.utile.WordWithCoef;

public class Runner {

	public static void main(String[] args) {

		WordAnalist wordAnalist = new WordAnalist(null, null);
		List<String> suffixes = new ArrayList<String>();
		suffixes.add(".html");
		suffixes.add(".htm");
		String pathInput = "D:/JavaProjectsIII/RIW/sites/try1/LifeWire/research.un.org/en/docs";
		String realTestPath = "D:/JavaProjectsIII/RIW/inputData/riwdataset";
		String testPath = "D:/JavaProjectsIII/RIW/testingOut";

		System.out.println("Starting up> " + new Date());
		DataCollector dataCollector = new DataCollector(realTestPath, 6);
		int nrOfFiles = dataCollector.getTotalFilesNumber();
		System.out.println("nrOfFiles:" + nrOfFiles);
		//

		while (true) {
			System.out.println();
			System.out.print("Search query:");
			Scanner scanner = new Scanner(System.in);

			String query = scanner.nextLine();
			// String query = "Advanced and Idioms and Functions";
			// String query = "verry or good or apples";
			System.out.println("Starting search> " + new Date());

			BooleanRetrieval booleanRetrieval = new BooleanRetrieval(query, nrOfFiles);
			Set<Integer> result = booleanRetrieval.executeQuery();
			Map<Integer, Set<WordWithCoef>> res = booleanRetrieval.getResult(result);

			VectorialRetrieval vr = new VectorialRetrieval(query, nrOfFiles);

			System.out.println("Rezultat: " + result);

			List<String> files = FileKeySolver.extractFilesWithKeysFromDB(result);
			System.out.println("Finish> " + new Date());
			if (files != null)
				for (String file : files)
					System.out.println(file);
			else
				System.out.println("nu sunt rezultate");
		}
	}

}
