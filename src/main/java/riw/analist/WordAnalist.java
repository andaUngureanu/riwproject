package riw.analist;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class WordAnalist {
	private static Set<String> stopWords = new HashSet<String>();
	private static Set<String> exceptionWords = new HashSet<String>();
	private static Set<String> operators = new HashSet<String>();

	public WordAnalist(String stopFile, String exceptionFile) {
		if (stopFile == null)
			stopFile = new String("wordsHelper/stopwords.txt");
		if (exceptionFile == null)
			exceptionFile = new String("wordsHelper/exceptionwords.txt");
		String line = "";

		try {

			BufferedReader bufferReader = new BufferedReader(new FileReader(stopFile));
			line = bufferReader.readLine();
			while (line != null) {
				if (line.length() > 2)
					stopWords.add(line);
				line = bufferReader.readLine();
			}
			bufferReader.close();

			bufferReader = new BufferedReader(new FileReader(exceptionFile));
			line = bufferReader.readLine();
			while (line != null) {
				exceptionWords.add(line);
				line = bufferReader.readLine();
			}
			bufferReader.close();
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		operators.add("and");
		operators.add("or");
		operators.add("not");
	}

	public static void printStopWords() {
		System.out.println(stopWords);
	}

	public static void printExceptionWords() {
		System.out.println(exceptionWords);
	}

	public static void printOperators() {
		System.out.println(operators);
	}

	public static boolean isExceptionWord(String word) {
		boolean check = exceptionWords.contains(word);
		return check;
	}

	public static boolean isStopWord(String word) {
		boolean check = stopWords.contains(word);
		return check;
	}

	public static boolean isOperator(String word) {
		boolean check = operators.contains(word);
		return check;
	}
}
