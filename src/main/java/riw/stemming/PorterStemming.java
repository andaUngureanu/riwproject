package riw.stemming;

public class PorterStemming {
	public static String aplayStemming(String word) {
		String outputWord = "";
		Stemmer s = new Stemmer();
		for (int i = 0; i < word.length(); ++i)
			s.add(word.charAt(i));
		s.stem();
		outputWord = s.toString();
		return outputWord;
	}
}
