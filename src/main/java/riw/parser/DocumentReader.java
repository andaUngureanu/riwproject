package riw.parser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class DocumentReader {
	String title = "";
	String keywords = "";
	String description = "";
	String robots = "";
	Set<String> links = new HashSet<String>();
	String text = "";

	public DocumentReader(String URL, String encoding, String baseUri) {
		File document = new File(URL);
		try {
			Document doc = Jsoup.parse(document, encoding, baseUri);
			title = doc.title();

			Elements elKeywords = doc.select("meta[name=\"keywords\"]");
			if (!elKeywords.isEmpty()) {
				keywords = doc.select("meta[name=\"keywords\"]").first().attr("content");
			}

			Elements elDescription = doc.select("meta[name=\"description\"]");
			if (!elDescription.isEmpty()) {
				description = doc.select("meta[name=\"description\"]").first().attr("content");
			}

			Elements elRobots = doc.select("meta[name=\"robots\"]");
			if (!elRobots.isEmpty()) {
				robots = elRobots.first().attr("content");
			}

			Elements linkElements = doc.select("a[href]");
			if (!linkElements.isEmpty()) {
				for (Element linkEl : linkElements) {
					//
					String link = linkEl.absUrl("href");
					links.add(link);

				}
			}

			text = doc.body().text();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createContentFile(String fileName) {
		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(fileName);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.println(title + "\n");
			printWriter.println(keywords + "\n");
			printWriter.println(description + "\n");
			printWriter.println(text);
			printWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void printLinksAndRobots() {
		System.out.println("Robots: " + robots);
		System.out.println("Links: ");
		for (String link : links) {
			System.out.println(link);
		}
	}

	// deprecated
	public static Map<String, Integer> computeTextFile(String fileName, int nrOfWords) {
		WordCounter wordCounter = new WordCounter();
		nrOfWords = wordCounter.ComputeTextFile(fileName);
		return wordCounter.getCountedWords();
	}
}