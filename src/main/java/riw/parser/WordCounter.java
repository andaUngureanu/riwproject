package riw.parser;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import riw.analist.WordAnalist;
import riw.stemming.PorterStemming;

public class WordCounter {
	private Map<String, Integer> countedWords = new HashMap<String, Integer>();

	public WordCounter() {

	}

	public Map<String, Integer> getCountedWords() {
		return countedWords;
	}

	public void setCountedWords(Map<String, Integer> data) {
		countedWords = data;
	}

	public static void ComputeTextInput(String input, List<String> words, List<String> operations) {
		String text = input.toLowerCase();
		String word = "";
		int startIdx = 0, endIdx = 0;
		for (int i = 0; i < text.length(); ++i) {
			if (!((text.charAt(i) >= 'a' && text.charAt(i) <= 'z')
					|| (text.charAt(i) >= '0' && text.charAt(i) <= '9'))) {
				endIdx = i;
				word = text.substring(startIdx, endIdx);
				if (WordAnalist.isOperator(word) && operations != null) {
					operations.add(word);
				} else if (WordAnalist.isExceptionWord(word)) {
					words.add(word);
				} else if (endIdx - startIdx > 2 && (!WordAnalist.isStopWord(word))) {
					String stemmedWord = PorterStemming.aplayStemming(word);
					words.add(stemmedWord);
				}
				startIdx = i + 1;
			}
		}
		word = text.substring(startIdx, text.length());
		if (WordAnalist.isExceptionWord(word)) {
			words.add(word);
		} else if (text.length() - startIdx > 2 && (!WordAnalist.isStopWord(word))) {
			String stemmedWord = PorterStemming.aplayStemming(word);
			words.add(stemmedWord);
		}
	}

	public int ComputeTextFile(String inputFile) {

		String text = "";
		try {
			text = new String(Files.readAllBytes(Paths.get(inputFile)));
		} catch (IOException e) {
			e.printStackTrace();
		}

		List<String> words = new ArrayList<String>();

		int startIdx = 0, endIdx = 0;

		ComputeTextInput(text, words, null);
		int nrOfWords = words.size();
		Collections.sort(words);
		for (String w : words) {
			if (countedWords.containsKey(w)) {
				countedWords.put(w, countedWords.get(w) + 1);
			} else {
				if (w.compareTo("") != 0) {
					countedWords.put(w, 1);
				}
			}
		}
		return nrOfWords;
	}

	public void WriteData() {
		System.out.println(countedWords);
	}

	public void WriteData(String fileName) {
		FileWriter fileWriter;
		try {
			fileWriter = new FileWriter(fileName);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.println(countedWords);
			printWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
