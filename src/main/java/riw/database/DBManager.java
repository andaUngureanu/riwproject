package riw.database;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class DBManager {
	private static final String dbName = "testindexerDB";
	private static final String hostName = "localhost";

	private static final String directindexName = "directindex";
	private static final String invertedindexName = "invertedindex";
	private static final String filesmapName = "filesmap";

	private static final int port = 27017;

	public static MongoDatabase db = null;

	private static MongoCollection<Document> directindex = null;
	private static MongoCollection<Document> invertedindex = null;
	private static MongoCollection<Document> filesmap = null;

	public static MongoCollection<Document> getFilesMap() {
		db = getDb();
		if (filesmap == null)
			filesmap = db.getCollection(filesmapName);
		return filesmap;
	}

	public static MongoCollection<Document> getDirectindex() {
		db = getDb();
		if (directindex == null)
			directindex = db.getCollection(directindexName);
		return directindex;
	}

	public static MongoCollection<Document> getInvertedindex() {
		db = getDb();
		if (invertedindex == null)
			invertedindex = db.getCollection(invertedindexName);
		return invertedindex;

	}

	public static String getDbName() {
		return dbName;
	}

	public static MongoDatabase getDb() {
		if (db == null)
			connect();
		return db;
	}

	public static void connect() {
		MongoClient mongoClient = new MongoClient(hostName, port);
		db = mongoClient.getDatabase(dbName);
	}
}
